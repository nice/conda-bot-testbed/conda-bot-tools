# conda-bot-tools

Some tools to help understand what the conda bot is doing

## Installation

```
conda env create -f environment.yml
```

### Usage

```
# after creating the environment
conda activate conda-bot-tools
jupyter lab visualize_conda_channel.ipynb
```